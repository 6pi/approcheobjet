package Domain;

public abstract class AbstractEvent {
	protected int capacity;
	
	public AbstractEvent(int capacity) {
		this.capacity = capacity;
	}
}
