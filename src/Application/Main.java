/*package Application;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import javafx.stage.Stage;

import Domain.AbstractEvent;
import Domain.Concert;
import Domain.Room;

public class Main {
	static void main(String args[]) {
		ArrayList<Room> room = new ArrayList<>();
		room.add(new Room(1500, LocalDate.of(2020, 5, 1), LocalTime.of(10, 0), LocalTime.of(16, 0)));
		room.add(new Room(1500, LocalDate.of(2020, 5, 1), LocalTime.of(10, 0), LocalTime.of(16, 0)));
		room.add(new Room(150000000, LocalDate.of(2020, 5, 1), LocalTime.of(10, 0), LocalTime.of(16, 0)));
		room.add(new Room(1500, LocalDate.of(2020, 5, 1), LocalTime.of(10, 0), LocalTime.of(16, 0)));

		ArrayList<AbstractEvent> events = new ArrayList<>();
		events.add(new Concert(LocalDate.of(2020, 11, 1) , "Prout Le Musical", 1500000));
		events.add(new Concert(LocalDate.of(2020, 9, 1) , "aba", 150));
		events.add(new Concert(LocalDate.of(2020, 6, 2) , "aba", 15));
		events.add(new Concert(LocalDate.of(2020, 6, 6) , "Micheeel Jacke", 15));
		
		return;
	}
}

*/

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}