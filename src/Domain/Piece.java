package Domain;

import java.time.LocalDate;
import java.util.ArrayList;

public class Piece extends AbstractEvent {
	ArrayList<LocalDate> date;
	String name;

	public Piece(ArrayList<LocalDate> date, String name, int capacity) {
		super(capacity);
		this.date = date;
		this.name = name;
	}	
}
