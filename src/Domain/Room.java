package Domain;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.util.HashMap;

public class Room {
	//Agreggate
	int capacity;
	LocalDate openDate;
	LocalTime openHour;
	LocalTime closeHour;
	HashMap<LocalDate, AbstractEvent> events;
	
	public Room(int capacity,  LocalDate openDate, LocalTime openHour, LocalTime closeHour) {
		this.capacity = capacity;
		this.openDate = openDate;
		this.openHour = openHour;
		this.closeHour = closeHour;
	}
	
	public boolean addEventOnDate(AbstractEvent e, LocalDate d) {
		if(d.compareTo(openDate) >= 0) {
			if (events.get(d) == null) {
				events.put(d, e);
				return true;
			}
		}
		return false;
	}
}
