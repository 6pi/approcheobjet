package Domain;

import java.time.LocalDate;

public class Concert extends AbstractEvent {
	LocalDate date;
	String name;
	
	public Concert(LocalDate date, String name, int capacity) {
		super(capacity);
		this.date = date;
		this.name = name;
	}
}
